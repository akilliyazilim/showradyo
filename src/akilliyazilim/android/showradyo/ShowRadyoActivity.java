package akilliyazilim.android.showradyo;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import akilliyazilim.android.showradyo.databases.ApplicationPreferences;
import akilliyazilim.android.showradyo.databases.MySQLiteOpenHelper;
import akilliyazilim.android.showradyo.fragments.DailyStreamListFragment;
import akilliyazilim.android.showradyo.fragments.FrequencyListFragment;
import akilliyazilim.android.showradyo.fragments.LiveStreamFragment;
import akilliyazilim.android.showradyo.fragments.MessageFragment;
import akilliyazilim.android.showradyo.fragments.TweetListFragment;
import akilliyazilim.android.showradyo.libraries.CustomViewPager;
import akilliyazilim.android.showradyo.playerservice.PlayerService;
import akilliyazilim.android.showradyo.utils.Utils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;

public class ShowRadyoActivity extends FragmentActivity implements
		OnTabChangeListener {

	private EasyTracker easyTracker = null;
	String tabFrequencyList = "a";
	String tabDailyStreamList = "a";
	String tabLiveStream = "a";
	String tabMessageForm = "a";
	String tabTweetList = "a";

	TabHost mTabHost;
	FrequencyListFragment frequencyListFragment;
	DailyStreamListFragment dailyStreamListFragment;
	LiveStreamFragment liveStreamFragment;
	MessageFragment messageFragment;
	TweetListFragment tweetListFragment;
	ShowRadyoPagerAdapter pageAdapter;
	CustomViewPager mViewPager;
	TabHost.TabSpec spec;
	ImageView imageview_play_pause_control;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MySQLiteOpenHelper helper = new MySQLiteOpenHelper(
				getApplicationContext());
		setContentView(R.layout.activity_show_radyo);
		easyTracker = EasyTracker.getInstance(ShowRadyoActivity.this);
		EasyTracker easyTracker = EasyTracker.getInstance(this);
		if (ApplicationPreferences.getInstance(getApplicationContext())
				.getDate().equals("0")) {
			ApplicationPreferences.getInstance(getApplicationContext())
					.saveDate(Utils.getDate());
			Log.i("ShowRadyoActivity", ApplicationPreferences.getInstance(getApplicationContext()).getDate() +"ilk tarih");
		}
		
		mViewPager = (CustomViewPager) findViewById(R.id.viewpager);
		mViewPager.setOffscreenPageLimit(5);

		frequencyListFragment = new FrequencyListFragment();
		dailyStreamListFragment = new DailyStreamListFragment();
		liveStreamFragment = new LiveStreamFragment();
		messageFragment = new MessageFragment();
		tweetListFragment = new TweetListFragment();

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setOnTabChangedListener(listener);
		mTabHost.setup();

		initializeTab();

		List<Fragment> fragments = getFragments();
		pageAdapter = new ShowRadyoPagerAdapter(getSupportFragmentManager(),
				fragments);
		mViewPager.setAdapter(pageAdapter);
		mTabHost.setCurrentTab(2);
	}

	public void initializeTab() {

		spec = mTabHost.newTabSpec(tabFrequencyList);
		mTabHost.setCurrentTab(-3);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabFrequencyList,
				R.drawable.tab_design_frequency));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabDailyStreamList);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabDailyStreamList,
				R.drawable.tab_design_daily_stream));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabLiveStream);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabLiveStream,
				R.drawable.tab_design_live_stream));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabMessageForm);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabMessageForm,
				R.drawable.tab_design_message));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabTweetList);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabTweetList,
				R.drawable.tab_design_tweet));
		mTabHost.addTab(spec);

		mTabHost.setOnTabChangedListener(this);
	}

	TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
		public void onTabChanged(String tabId) {
			/* Set current tab.. */
			if (tabId.equals(tabFrequencyList)) {
				pushFragments(tabId, frequencyListFragment);
			} else if (tabId.equals(tabDailyStreamList)) {
				pushFragments(tabId, dailyStreamListFragment);
			} else if (tabId.equals(tabLiveStream)) {
				pushFragments(tabId, liveStreamFragment);
			} else if (tabId.equals(tabMessageForm)) {
				pushFragments(tabId, messageFragment);
			} else if (tabId.equals(tabTweetList)) {
				pushFragments(tabId, tweetListFragment);
			}
		}
	};

	public void pushFragments(String tag, Fragment fragment) {

		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();

		ft.replace(android.R.id.tabcontent, fragment);
		ft.commit();
	}

	private View createTabView(final String text, final int id) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
		imageView.setImageDrawable(getResources().getDrawable(id));
		return view;
	}

	@Override
	public void onTabChanged(String tag) {
		int pos = this.mTabHost.getCurrentTab();
		this.mViewPager.setCurrentItem(pos);

	}

	private List<Fragment> getFragments() {
		List<Fragment> fList = new ArrayList<Fragment>();

		// TODO Put here your Fragments
		FrequencyListFragment f1 = FrequencyListFragment
				.newInstance("Sample Fragment 1");
		DailyStreamListFragment f2 = DailyStreamListFragment.newInstance(
				"Sample Fragment 2", getApplicationContext());
		LiveStreamFragment f3 = LiveStreamFragment
				.newInstance("Sample Fragment 3");
		MessageFragment f4 = MessageFragment.newInstance("Sample Fragment 4");
		TweetListFragment f5 = TweetListFragment
				.newInstance("Sample Fragment 5");
		fList.add(f1);
		fList.add(f2);
		fList.add(f3);
		fList.add(f4);
		fList.add(f5);

		return fList;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		EasyTracker.getInstance(getApplicationContext()).activityStart(this);
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		EasyTracker.getInstance(this).activityStop(this);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		PlayerService.mNotificationManager.cancel(PlayerService.HELLO_ID);
	}

}
