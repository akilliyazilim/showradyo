package akilliyazilim.android.showradyo;

import akilliyazilim.android.showradyo.databases.MySQLiteOpenHelper;
import akilliyazilim.android.showradyo.utils.Utils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

public class ShowRadyoSplashActivity extends Activity {
	
	ImageView splashImageView;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		

		splashImageView = (ImageView) findViewById(R.id.imageview_splash);
		MySQLiteOpenHelper helper = new MySQLiteOpenHelper(getApplicationContext());
		SQLiteDatabase db = helper.getWritableDatabase();
		db.close();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if(Utils.checkConn(getApplicationContext()))
				{
					double device_inch = Utils.getDeviceInches(ShowRadyoSplashActivity.this);
					Log.v("DEVICE_ICNHES", String.valueOf(device_inch));
					
					if(device_inch<(5.65))
					{
						splashImageView.setBackgroundResource(R.drawable.splash_background);
						splashImageView.setScaleType(ScaleType.CENTER_CROP);
						Intent intentSplash = new Intent(ShowRadyoSplashActivity.this,ShowRadyoActivity.class);
						startActivity(intentSplash);
					}
					else
					{
						splashImageView.setBackgroundResource(R.drawable.splash_background_tablet);
						splashImageView.setScaleType(ScaleType.CENTER_CROP);
						Intent intentSplash = new Intent(ShowRadyoSplashActivity.this,ShowRadyoTabletActivity.class);
						startActivity(intentSplash);
					}
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Internet bağlantınızı kontrol ediniz.", Toast.LENGTH_SHORT).show();
				}
			}
		}, 3000);
	}
}
