package akilliyazilim.android.showradyo;

import java.util.ArrayList;
import java.util.List;

import akilliyazilim.android.showradyo.fragments.DailyStreamListFragment;
import akilliyazilim.android.showradyo.fragments.FrequencyListFragment;
import akilliyazilim.android.showradyo.fragments.LiveStreamFragment;
import akilliyazilim.android.showradyo.fragments.MessageFragment;
import akilliyazilim.android.showradyo.fragments.TweetListFragment;
import akilliyazilim.android.showradyo.libraries.CustomViewPager;
import akilliyazilim.android.showradyo.playerservice.PlayerService;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

public class ShowRadyoTabletActivity extends FragmentActivity implements OnTabChangeListener  {

	
	String tabFrequencyList = "a";
	String tabDailyStreamList ="a";
	String tabMessageForm ="a";
	String tabTweetList ="a";

	TabHost mTabHost;
	FrequencyListFragment frequencyListFragment;
	DailyStreamListFragment dailyStreamListFragment;
	LiveStreamFragment liveStreamFragment;
	MessageFragment messageFragment;
	TweetListFragment tweetListFragment;
	ShowRadyoPagerAdapter pageAdapter;
	CustomViewPager mViewPager;
	TabHost.TabSpec spec;
    ImageView imageview_play_pause_control;
    FragmentManager fragmentMan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_tablet_show_radyo);

		mViewPager = (CustomViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(4);
        
        fragmentMan = getSupportFragmentManager();

        
		frequencyListFragment = new FrequencyListFragment();
		dailyStreamListFragment = new DailyStreamListFragment();
		messageFragment = new MessageFragment();
		tweetListFragment = new TweetListFragment();
		liveStreamFragment = new LiveStreamFragment();
		
		
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setOnTabChangedListener(listener);
		mTabHost.setup();

		initializeTab();
		
		fragmentMan.beginTransaction().replace(R.id.live_stream_tablet, liveStreamFragment).commit();

		List<Fragment> fragments = getFragments();
		pageAdapter = new ShowRadyoPagerAdapter(getSupportFragmentManager(), fragments);
		mViewPager.setAdapter(pageAdapter);
		
		
	}
	
	public void initializeTab() {

		spec = mTabHost.newTabSpec(tabFrequencyList);
		mTabHost.setCurrentTab(-3);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabFrequencyList, R.drawable.tab_design_frequency));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabDailyStreamList);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabDailyStreamList, R.drawable.tab_design_daily_stream));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabMessageForm);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabMessageForm, R.drawable.tab_design_message));
		mTabHost.addTab(spec);

		spec = mTabHost.newTabSpec(tabTweetList);
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(android.R.id.tabcontent);
			}
		});
		spec.setIndicator(createTabView(tabTweetList, R.drawable.tab_design_tweet));
		mTabHost.addTab(spec);

		mTabHost.setOnTabChangedListener(this);
	}

	TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
		public void onTabChanged(String tabId) {
			/* Set current tab.. */
			if (tabId.equals(tabFrequencyList)) {
				pushFragments(tabId, frequencyListFragment);
			} else if (tabId.equals(tabDailyStreamList)) {
				pushFragments(tabId, dailyStreamListFragment);
			}else if (tabId.equals(tabMessageForm)) {
				pushFragments(tabId, messageFragment);
			} else if (tabId.equals(tabTweetList)) {
				pushFragments(tabId, tweetListFragment);
			}
		}
	};

	public void pushFragments(String tag, Fragment fragment) {

		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();

		ft.replace(android.R.id.tabcontent, fragment);
		ft.commit();
	}

	private View createTabView(final String text, final int id) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
		imageView.setImageDrawable(getResources().getDrawable(id));
		return view;
	}

	@Override
	public void onTabChanged(String tag) {
		int pos = this.mTabHost.getCurrentTab();
		this.mViewPager.setCurrentItem(pos);

	}

	private List<Fragment> getFragments() {
		List<Fragment> fList = new ArrayList<Fragment>();

		// TODO Put here your Fragments
		FrequencyListFragment f1 = FrequencyListFragment.newInstance("Sample Fragment 1");
		DailyStreamListFragment f2 = DailyStreamListFragment.newInstance("Sample Fragment 2",getApplicationContext());
		MessageFragment f4 = MessageFragment.newInstance("Sample Fragment 4");
		TweetListFragment f5 = TweetListFragment.newInstance("Sample Fragment 5");
		fList.add(f1);
		fList.add(f2);
		fList.add(f4);
		fList.add(f5);

		return fList;
	}

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		PlayerService.mNotificationManager.cancel(PlayerService.HELLO_ID);
	}
	

}
