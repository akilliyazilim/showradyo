package akilliyazilim.android.showradyo.adapters;

import java.util.ArrayList;

import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.model.FrequencyListItem;
import android.content.Context;

public class FrequencyCityWheelAdapter extends AbstractWheelTextAdapter {
	
	ArrayList<FrequencyListItem> frequencyArrayList;
	
	public FrequencyCityWheelAdapter(Context context,ArrayList<FrequencyListItem> frequencyArrayList) {
		super(context,R.layout.layout_inflate_frequency_wheel,NO_RESOURCE);
		setItemTextResource(R.id.wheel_text);
		this.frequencyArrayList=frequencyArrayList;
	}

	@Override
	public int getItemsCount() {
		return frequencyArrayList.size();
	}

	@Override
	protected CharSequence getItemText(int index) {
		return frequencyArrayList.get(index).getCityName();
	}
}
