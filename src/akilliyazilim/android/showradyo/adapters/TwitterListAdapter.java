package akilliyazilim.android.showradyo.adapters;

import java.util.ArrayList;
import java.util.List;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.model.Tweet;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TwitterListAdapter extends BaseAdapter{
	
	private ArrayList<Tweet> mTweetList;

	private LayoutInflater mLayoutInflater;

	public void addItems(List<Tweet> newItems) {
		if (null == newItems || newItems.size() <= 0) {
			return;
		}

		if (null == mTweetList) {
			mTweetList = new ArrayList<Tweet>();
		}

		mTweetList.addAll(newItems);
		notifyDataSetChanged();
	}

	public TwitterListAdapter(Context context, ArrayList<Tweet> arrayListTweet) {

		mTweetList = arrayListTweet;

		// get the layout inflater
		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub

		return mTweetList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (view == null) {
			holder = new ViewHolder();

			view = mLayoutInflater.inflate(R.layout.tweet_list_item, null);
			holder.tweet = (TextView) view.findViewById(R.id.tweet);
			holder.time = (TextView)view.findViewById(R.id.time);
			view.setTag(holder);
		} else {

			holder = (ViewHolder) view.getTag();
		}

		String stringTweet = mTweetList.get(position).getTweet();
		String stringTime = mTweetList.get(position).getTweet_date();
		if (stringTweet != null && stringTime != null) {
			if (holder.tweet != null) {

				holder.tweet.setText(stringTweet);
				holder.time.setText(stringTime);
			}
		}

		return view;

	}

	private static class ViewHolder {

		protected TextView tweet;
		protected TextView time;

	}

}
