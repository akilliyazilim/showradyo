package akilliyazilim.android.showradyo.adapters;

import java.util.ArrayList;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.databases.MySQLiteOpenHelper;
import akilliyazilim.android.showradyo.model.DailyStreamListItem;
import akilliyazilim.android.showradyo.model.ViewHolderDailyList;
import akilliyazilim.android.showradyo.utils.ImageLoader;
import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DailyStreamListAdapter extends BaseAdapter {

	ArrayList<DailyStreamListItem> arraylistDailyStream;
	Context context;
	LayoutInflater inflater;
	String imageDjLink;
	ImageLoader image_loader;
	Activity activity;
	int controlDB=0;

	public DailyStreamListAdapter(Context context,
			ArrayList<DailyStreamListItem> arraylistDailyStream,
			Activity activity,int controlDB) {
		this.context = context;
		this.activity = activity;
		this.controlDB = controlDB;
		this.arraylistDailyStream = arraylistDailyStream;
		Log.i("123456789",arraylistDailyStream.get(arraylistDailyStream.size()-1).getDjName() +"*+--+*" + arraylistDailyStream.get(arraylistDailyStream.size()-1).getDayName() +"*+--+*"+arraylistDailyStream.get(arraylistDailyStream.size()-1).getProgramName());

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		image_loader = new ImageLoader(context, activity);
	}

	@Override
	public int getCount() {
		return arraylistDailyStream.size();
	}

	@Override
	public Object getItem(int position) {
		return arraylistDailyStream.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDailyList holder;
		
		if(convertView==null){
			LayoutInflater inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.layout_inflate_daily_item, parent, false);
			holder = new ViewHolderDailyList();
			holder.endTime = (TextView) convertView.findViewById(R.id.end_time);
					holder.nameDJ=(TextView) convertView.findViewById(R.id.dj_name);
					holder.nameProgram= (TextView) convertView.findViewById(R.id.program_name);
					holder.startTime=(TextView) convertView.findViewById(R.id.start_time);
					holder.imageDJ = (ImageView) convertView.findViewById(R.id.daily_dj_image);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolderDailyList) convertView.getTag();
		}
		
//		View view = inflater.inflate(R.layout.layout_inflate_daily_item,
//				parent, false);
//		TextView startTime = (TextView) view.findViewById(R.id.start_time);
//		TextView endTime = (TextView) view.findViewById(R.id.end_time);
//		TextView nameDJ = (TextView) view.findViewById(R.id.dj_name);
//		TextView nameProgram = (TextView) view.findViewById(R.id.program_name);
//		ImageView imageDJ = (ImageView) view.findViewById(R.id.daily_dj_image);
//
		holder.startTime.setText(arraylistDailyStream.get(position).getStartTime());
		holder.endTime.setText(arraylistDailyStream.get(position).getEndTime());
		holder.nameDJ.setText(arraylistDailyStream.get(position).getDjName());
		holder.nameProgram
				.setText(arraylistDailyStream.get(position).getProgramName());
		imageDjLink = arraylistDailyStream.get(position).getDjMobilImages();
		
		MySQLiteOpenHelper helper = new MySQLiteOpenHelper(context);
		SQLiteDatabase db = helper.getWritableDatabase();
		Log.i("123456789",arraylistDailyStream.get(position).getDjName() +"+--+" + arraylistDailyStream.get(position).getDayName() +"+--+"+arraylistDailyStream.get(position).getProgramName());
		helper.addDJ(arraylistDailyStream.get(position), db);
		helper.close();
		image_loader.DisplayImage(imageDjLink, holder.imageDJ, 128, 128,
				arraylistDailyStream.get(position),arraylistDailyStream.get(position).getID());
		
		return convertView;
	}

}
