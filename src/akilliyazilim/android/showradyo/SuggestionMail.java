package akilliyazilim.android.showradyo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SuggestionMail extends Activity {
	
	TextView textview_send,textview_back;
	EditText edittext_title,edittext_message_content;
	String macAddress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout_suggestion_mail);
		
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		macAddress = wInfo.getMacAddress(); 
		
		textview_back = (TextView) findViewById(R.id.textview_vazgec);
		textview_send = (TextView) findViewById(R.id.textview_gonder);
		edittext_message_content = (EditText) findViewById(R.id.edittext_i_message_content);
		edittext_title = (EditText) findViewById(R.id.edittext_konu);
		
		textview_send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String title = edittext_title.getText().toString();
				String message = edittext_message_content.getText().toString();
				if(message.length()>10 && title.length()>3)
				{
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("message/rfc822");
					i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"destek@showradyo.com.tr"});
					i.putExtra(Intent.EXTRA_SUBJECT, title);
					i.putExtra(Intent.EXTRA_TEXT   , message);
					try {
					    startActivity(Intent.createChooser(i, "Send mail..."));
					} catch (android.content.ActivityNotFoundException ex) {
					    Toast.makeText(SuggestionMail.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
					}
				}
				else
					Toast.makeText(getApplicationContext(), "Boşlukları doldurunuz.", Toast.LENGTH_SHORT).show();
			}
		});
		
		textview_back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		
	}
	
	public class SendMessageAsyncTask extends AsyncTask<Void, Void, Void>
	{
		Context context;
		String title,message,mail;

		public SendMessageAsyncTask(Context context,String title,String message,String mail) {
			this.context=context;
			this.title=title;
			this.message=message;
			this.mail=mail;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			
			return null;
		}
		
	}

}
