package akilliyazilim.android.showradyo.model;

public class LiveStreamInfo {
	
	int LiveID;
	String CompanyID,CompanyName,LiveStreamName,LiveStreamLink,LiveStreamDescription;
	
	public int getLiveID() {
		return LiveID;
	}
	public void setLiveID(int liveID) {
		LiveID = liveID;
	}
	public String getCompanyID() {
		return CompanyID;
	}
	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public String getLiveStreamName() {
		return LiveStreamName;
	}
	public void setLiveStreamName(String liveStreamName) {
		LiveStreamName = liveStreamName;
	}
	public String getLiveStreamLink() {
		return LiveStreamLink;
	}
	public void setLiveStreamLink(String liveStreamLink) {
		LiveStreamLink = liveStreamLink;
	}
	public String getLiveStreamDescription() {
		return LiveStreamDescription;
	}
	public void setLiveStreamDescription(String liveStreamDescription) {
		LiveStreamDescription = liveStreamDescription;
	}
	
	

}
