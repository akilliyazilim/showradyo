package akilliyazilim.android.showradyo.model;

public class DailyStreamListItem {
	int dailyStreamID;
	String CompanyID, CompanyName, DayName, DjImage, StartTime, EndTime,
			ProgramName, ProgramContent, DjName, DjFacebookLink, DjTwitterLink,
			DjCartoonImages, HashTag, DjMobilImages, Torder, Day;
	String ID;

	public String getID() {
		return ID;
	}

	public void setID(String id) {
		ID = id;
	}

	public String getDay() {
		return Day;
	}

	public void setDay(String day) {
		Day = day;
	}

	public int getDailyStreamID() {
		return dailyStreamID;
	}

	public void setDailyStreamID(int dailyStreamID) {
		this.dailyStreamID = dailyStreamID;
	}

	public String getCompanyID() {
		return CompanyID;
	}

	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getDayName() {
		return DayName;
	}

	public void setDayName(String dayName) {
		DayName = dayName;
	}

	public String getDjImage() {
		return DjImage;
	}

	public void setDjImage(String djImage) {
		DjImage = djImage;
	}

	public String getStartTime() {
		return StartTime;
	}

	public void setStartTime(String startTime) {
		StartTime = startTime;
	}

	public String getEndTime() {
		return EndTime;
	}

	public void setEndTime(String endTime) {
		EndTime = endTime;
	}

	public String getProgramName() {
		return ProgramName;
	}

	public void setProgramName(String programName) {
		ProgramName = programName;
	}

	public String getProgramContent() {
		return ProgramContent;
	}

	public void setProgramContent(String programContent) {
		ProgramContent = programContent;
	}

	public String getDjName() {
		return DjName;
	}

	public void setDjName(String djName) {
		DjName = djName;
	}

	public String getDjFacebookLink() {
		return DjFacebookLink;
	}

	public void setDjFacebookLink(String djFacebookLink) {
		DjFacebookLink = djFacebookLink;
	}

	public String getDjTwitterLink() {
		return DjTwitterLink;
	}

	public void setDjTwitterLink(String djTwitterLink) {
		DjTwitterLink = djTwitterLink;
	}

	public String getDjCartoonImages() {
		return DjCartoonImages;
	}

	public void setDjCartoonImages(String djCartoonImages) {
		DjCartoonImages = djCartoonImages;
	}

	public String getHashTag() {
		return HashTag;
	}

	public void setHashTag(String hashTag) {
		HashTag = hashTag;
	}

	public String getDjMobilImages() {
		return DjMobilImages;
	}

	public void setDjMobilImages(String djMobilImages) {
		DjMobilImages = djMobilImages;
	}

	public String getTorder() {
		return Torder;
	}

	public void setTorder(String torder) {
		Torder = torder;
	}

}
