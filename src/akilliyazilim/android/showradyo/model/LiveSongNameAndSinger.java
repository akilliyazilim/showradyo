package akilliyazilim.android.showradyo.model;

public class LiveSongNameAndSinger {
	String SongName,SingerName,RadyoName;

	public String getSongName() {
		return SongName;
	}

	public void setSongName(String songName) {
		SongName = songName;
	}

	public String getSingerName() {
		return SingerName;
	}

	public void setSingerName(String singerName) {
		SingerName = singerName;
	}

	public String getRadyoName() {
		return RadyoName;
	}

	public void setRadyoName(String radyoName) {
		RadyoName = radyoName;
	}
	
	
	
	

}
