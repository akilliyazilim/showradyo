package akilliyazilim.android.showradyo.model;

public class FrequencyListItem {
	int ID;
	String CompanyID;
	String CompanyName;
	String CityName;
	String FrequencyNumber;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getCompanyID() {
		return CompanyID;
	}
	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public String getCityName() {
		return CityName;
	}
	public void setCityName(String cityName) {
		CityName = cityName;
	}
	public String getFrequencyNumber() {
		return FrequencyNumber;
	}
	public void setFrequencyNumber(String frequencyNumber) {
		FrequencyNumber = frequencyNumber;
	}
	
	

}
