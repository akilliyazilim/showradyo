package akilliyazilim.android.showradyo.webservicedata;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import akilliyazilim.android.showradyo.model.CurrentDj;
import akilliyazilim.android.showradyo.model.DailyStreamListItem;
import akilliyazilim.android.showradyo.model.FrequencyListItem;
import akilliyazilim.android.showradyo.model.LiveSongNameAndSinger;
import akilliyazilim.android.showradyo.model.LiveStreamInfo;
import android.util.Log;
import android.widget.Toast;


public class DataService {
	
	private static String SHOW_SERV�CE_URL = "http://showradyo.com.tr/service/showcanli.asmx";
	private final String NAMESPACE = "http://tempuri.org/";
	private final String SOAP_ACTION = "http://tempuri.org/";
	
	//Frequency list tags
	private static String TAG_NAME_COMPANY_ID = "CompanyID";
	private static String TAG_NAME_ID = "CompanyID";
	private static String TAG_NAME_COMPANY_NAME = "CompanyName";
	private static String TAG_NAME_CITY_NAME = "CityName";
	private static String TAG_NAME_FREQUENCY_NUMBER = "FrequencyNumber";
	
	//DailyStream list tags
	private static String TAG_NAME_DAILYSTREAM_ID = "ID";
	private static String TAG_NAME_DAY_NAME = "DayName";
	private static String TAG_NAME_DJ_IMAGE = "DjImage";
	private static String TAG_NAME_STAR_TIME = "StartTime";
	private static String TAG_NAME_END_TIME = "EndTime";
	private static String TAG_NAME_PROGRAM_NAME = "ProgramName";
	private static String TAG_NAME_PROGRAM_CONTENT = "ProgramContent";
	private static String TAG_NAME_DJ_NAME = "DjName";
	private static String TAG_NAME_DJ_FACEBOOK_LINK = "DjFacebookLink";
	private static String TAG_NAME_DJ_TWITTER_LINK = "DjTwitterLink";
	private static String TAG_NAME_DJ_CARTOON_IMAGE = "DjCartoonImages";
	private static String TAG_NAME_HASHTAG = "HashTag";
	private static String TAG_NAME_DJ_MOBIL_IMAGES = "DjMobilImages";
	private static String TAG_NAME_TORDER = "Torder";
	private static String TAG_NAME_TODAY = "Today";
	
	//Live song and singer tags
	private static String TAG_NAME_SONG_NAME = "SongName";
	private static String TAG_NAME_SINGER_NAME = "SingerName";
	private static String TAG_NAME_RADYO_NAME = "RadyoName";
	
	//Live stream info tags
	private static String TAG_NAME_LIVE_STREAM_LINK = "LiveStreamLink";
	private static String TAG_NAME_LIVE_STREAM_DESCRIPTION = "LiveStreamDescription";
	private static String TAG_NAME_LIVE_STREAM_NAME = "LiveStreamName";
	private static String TAG_NAME_LIVE_STREAM_NAME_OS = "ANDROID";
	private static String TAG_NAME_LIVE_STREAM_NAME_WIMAP = "WINAMP";
	
	//message form tags
	private static String TAG_NAME_MESSAGE_TITLE = "Title";
	private static String TAG_NAME_MESSAGE_CONTENT ="MessageContent";
	private static String TAG_NAME_MAIL_ADRESS = "mailAdress";
	private static String TAG_NAME_DEVICE_ID = "DeviceID";
	
	
	//Method Names Tags
	private static String TAG_METHOD_NAME_FREQUENCY_LIST = "GetOnFrequencyList";
	private static String TAG_METHOD_NAME_DAILY_STREAM = "GetOnDailyStream";
	private static String TAG_METHOD_NAME_LIVE_STREAM_LIST = "GetOnLiveStreamList";
	private static String TAG_METHOD_NAME_MESSAGE_FORM = "GetOnMessageForm";
	private static String TAG_METHOD_NAME_LIVE_SONG_NAME_SINGER = "GetOnShowRadyoLiveSongNameAndSinger";
	private static String TAG_METHOD_NAME_CURRENT_DJ_INFO = "GetOnShowCurrentDjInfo";
	
	
	private static DataService instance=null;
	
	public static DataService getInstance()
	{
		if(instance==null)
			instance= new DataService();
		return instance;
	}

	public ArrayList<FrequencyListItem> getFrequencyList(String CompanyID) throws JSONException
	{
		ArrayList<FrequencyListItem> frequencyArrayList=null;
		JSONArray jsonArray ;
		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_FREQUENCY_LIST);
		PropertyInfo paramPI = new PropertyInfo();
		paramPI.setName(TAG_NAME_COMPANY_ID);
		paramPI.setValue(CompanyID);
		paramPI.setType(String.class);
		
		request.addProperty(paramPI);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);

		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_FREQUENCY_LIST, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			String soapResponseString = response.toString();
			jsonArray = new JSONArray(soapResponseString);
			
			frequencyArrayList = new ArrayList<FrequencyListItem>();
			JSONObject objJson;
			for (int i = 0; i < jsonArray.length(); i++) {
				objJson = jsonArray.getJSONObject(i);
				FrequencyListItem item = new FrequencyListItem();
				item.setCityName(objJson.getString(TAG_NAME_CITY_NAME));
				item.setCompanyID(objJson.getString(TAG_NAME_COMPANY_ID));
				item.setCompanyName(objJson.getString(TAG_NAME_COMPANY_NAME));
				item.setFrequencyNumber(objJson.getString(TAG_NAME_FREQUENCY_NUMBER));
				item.setID(objJson.getInt(TAG_NAME_ID));
				frequencyArrayList.add(item);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return frequencyArrayList;
	}

	public ArrayList<DailyStreamListItem> getDailyStreamList(String Today,String CompanyID)
	{
		ArrayList<DailyStreamListItem> dailyStreamArrayList=null;
		JSONArray jsonArray ;

		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_DAILY_STREAM);
		
		request.addProperty(TAG_NAME_TODAY,Today);
		request.addProperty(TAG_NAME_COMPANY_ID,CompanyID);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_DAILY_STREAM, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			String soapResponseString = response.toString();
			jsonArray = new JSONArray(soapResponseString);
			
			dailyStreamArrayList = new ArrayList<DailyStreamListItem>();
			JSONObject objJson;
			for (int i = 0; i < jsonArray.length(); i++) {
				
				objJson = jsonArray.getJSONObject(i);
				DailyStreamListItem item = new DailyStreamListItem();
				item.setID(String.valueOf(objJson.getInt(TAG_NAME_DAILYSTREAM_ID)));
				item.setCompanyID(objJson.getString(TAG_NAME_COMPANY_ID));
				item.setCompanyName(objJson.getString(TAG_NAME_COMPANY_NAME));
				item.setDailyStreamID(objJson.getInt(TAG_NAME_ID));
				item.setDayName(objJson.getString(TAG_NAME_DAY_NAME));
				item.setDjCartoonImages(objJson.getString(TAG_NAME_DJ_CARTOON_IMAGE));
				item.setDjFacebookLink(objJson.getString(TAG_NAME_DJ_FACEBOOK_LINK));
				item.setDjImage(objJson.getString(TAG_NAME_DJ_IMAGE));
				item.setDjMobilImages(objJson.getString(TAG_NAME_DJ_MOBIL_IMAGES));
				item.setDjName(objJson.getString(TAG_NAME_DJ_NAME));
				item.setDjTwitterLink(objJson.getString(TAG_NAME_DJ_TWITTER_LINK));
				item.setEndTime(objJson.getString(TAG_NAME_END_TIME));
				item.setHashTag(objJson.getString(TAG_NAME_HASHTAG));
				item.setProgramContent(objJson.getString(TAG_NAME_PROGRAM_CONTENT));
				item.setProgramName(objJson.getString(TAG_NAME_PROGRAM_NAME));
				item.setStartTime(objJson.getString(TAG_NAME_STAR_TIME));
				item.setTorder(objJson.getString(TAG_NAME_TORDER));
				item.setDay(Today);
				dailyStreamArrayList.add(item);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.v("SIZE", e.getLocalizedMessage());
		}

		Log.v("SIZE", String.valueOf(dailyStreamArrayList.size()));
		return dailyStreamArrayList;
	}

	public LiveSongNameAndSinger getLiveSongNameAndSinger()
	{
		LiveSongNameAndSinger liveSong = null;
		JSONObject jsonobj=null;
		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_LIVE_SONG_NAME_SINGER);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);
		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_LIVE_SONG_NAME_SINGER, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			String soapResponseString = response.toString();
			jsonobj = new JSONObject(soapResponseString);
			
			liveSong = new LiveSongNameAndSinger();
			liveSong.setRadyoName(jsonobj.getString(TAG_NAME_RADYO_NAME));
			liveSong.setSingerName(jsonobj.getString(TAG_NAME_SINGER_NAME));
			liveSong.setSongName(jsonobj.getString(TAG_NAME_SONG_NAME));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return liveSong;
	}
	
	public CurrentDj getCurrentDjInfo()
	{
		CurrentDj currentDj = null;
		
		JSONArray json_array=null;
		JSONObject jsonobj=null;
		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_CURRENT_DJ_INFO);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);
		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_CURRENT_DJ_INFO, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			String soapResponseString = response.toString();
			json_array = new JSONArray(soapResponseString);
			jsonobj = json_array.getJSONObject(0);
			
			currentDj = new CurrentDj();
			currentDj.setCompanyID(jsonobj.getString(TAG_NAME_COMPANY_ID));
			currentDj.setCompanyName(jsonobj.getString(TAG_NAME_COMPANY_NAME));
			currentDj.setCurrentDjID(jsonobj.getInt(TAG_NAME_ID));
			currentDj.setDayName(jsonobj.getString(TAG_NAME_DAY_NAME));
			currentDj.setDjCartoonImages(jsonobj.getString(TAG_NAME_DJ_CARTOON_IMAGE));
			currentDj.setDjFacebookLink(jsonobj.getString(TAG_NAME_DJ_FACEBOOK_LINK));
			currentDj.setDjImage(jsonobj.getString(TAG_NAME_DJ_IMAGE));
			currentDj.setDjMobilImages(jsonobj.getString(TAG_NAME_DJ_MOBIL_IMAGES));
			currentDj.setDjName(jsonobj.getString(TAG_NAME_DJ_NAME));
			currentDj.setDjTwitterLink(jsonobj.getString(TAG_NAME_DJ_TWITTER_LINK));
			currentDj.setEndTime(jsonobj.getString(TAG_NAME_END_TIME));
			currentDj.setHashTag(jsonobj.getString(TAG_NAME_HASHTAG));
			currentDj.setProgramContent(jsonobj.getString(TAG_NAME_PROGRAM_CONTENT));
			currentDj.setProgramName(jsonobj.getString(TAG_NAME_PROGRAM_NAME));
			currentDj.setStartTime(jsonobj.getString(TAG_NAME_STAR_TIME));
			currentDj.setTorder(jsonobj.getString(TAG_NAME_TORDER));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return currentDj;
				
	}

	public LiveStreamInfo getLiveStreamInfo(String CompanyID)
	{
		LiveStreamInfo liveStreamInfo = null;
		
		JSONArray json_array=null;
		JSONObject jsonobj=null;
		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_LIVE_STREAM_LIST);

		PropertyInfo paramPI = new PropertyInfo();
		paramPI.setName(TAG_NAME_COMPANY_ID);
		paramPI.setValue(CompanyID);
		paramPI.setType(String.class);
		
		request.addProperty(paramPI);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);
		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_LIVE_STREAM_LIST, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			String soapResponseString = response.toString();
			json_array = new JSONArray(soapResponseString);

			Log.v("DAILYSTREAMRESULT", String.valueOf(json_array.length()));
			
			for (int i = 0; i < json_array.length(); i++) {

				jsonobj = json_array.getJSONObject(i);
				if(jsonobj.getString(TAG_NAME_LIVE_STREAM_NAME).equals(TAG_NAME_LIVE_STREAM_NAME_WIMAP))
				{
					liveStreamInfo = new LiveStreamInfo();
					liveStreamInfo.setLiveID(jsonobj.getInt(TAG_NAME_ID));
					liveStreamInfo.setCompanyName(jsonobj.getString(TAG_NAME_COMPANY_NAME));
					liveStreamInfo.setCompanyID(jsonobj.getString(TAG_NAME_COMPANY_ID));
					liveStreamInfo.setLiveStreamDescription(jsonobj.getString(TAG_NAME_LIVE_STREAM_DESCRIPTION));
					liveStreamInfo.setLiveStreamLink(jsonobj.getString(TAG_NAME_LIVE_STREAM_LINK));
					liveStreamInfo.setLiveStreamName(jsonobj.getString(TAG_NAME_LIVE_STREAM_NAME));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(liveStreamInfo==null)
			return null;
		return liveStreamInfo;
	}

	public boolean sendMessageForm(String CompanyID,String CompanyName,String Title,String MessageContent,String mailAdress,String DeviceID)
	{
		
		SoapObject request = new SoapObject(NAMESPACE, TAG_METHOD_NAME_MESSAGE_FORM);
		String soapResponseString = "";
		
		request.addProperty(TAG_NAME_COMPANY_ID,CompanyID);
		request.addProperty(TAG_NAME_COMPANY_NAME,CompanyName);
		request.addProperty(TAG_NAME_MESSAGE_TITLE,Title);
		request.addProperty(TAG_NAME_MESSAGE_CONTENT,MessageContent);
		request.addProperty(TAG_NAME_MAIL_ADRESS,mailAdress);
		request.addProperty(TAG_NAME_DEVICE_ID,DeviceID);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(SHOW_SERV�CE_URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION+TAG_METHOD_NAME_MESSAGE_FORM, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			
			soapResponseString= response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(soapResponseString.equals("true"))
			return true;
		return false;
	}
}
