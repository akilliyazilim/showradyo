package akilliyazilim.android.showradyo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;

public class Utils {
	
	public static Bitmap getResizedImage(Context context,Bitmap bm,int newHeight,int newWidth)
	{
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) newWidth) / width;
	    float scaleHeight = ((float) newHeight) / height;
	    // create a matrix for the manipulation
	    Matrix matrix = new Matrix();
	    // resize the bit map
	    matrix.postScale(scaleWidth, scaleHeight);
	    // recreate the new Bitmap
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	    return resizedBitmap;
	}
	
	public static Bitmap getCroppedImage(Context context,Bitmap image)
	{
			Paint strokePaint = new Paint();
			strokePaint.setDither(true);
			strokePaint.setColor(Color.rgb(250, 250, 250));
			
	
			strokePaint.setStyle(Paint.Style.STROKE);
			strokePaint.setAntiAlias(true);
			strokePaint.setStrokeWidth(3);
			Bitmap circle_bitmap = Bitmap.createBitmap(image.getWidth(),
					image.getHeight(), Bitmap.Config.ARGB_8888);

			BitmapShader bitmap_shader = new BitmapShader(image, TileMode.CLAMP,
					TileMode.CLAMP);
			Paint paint = new Paint();
			paint.setShader(bitmap_shader);

			Canvas c = new Canvas(circle_bitmap);
			c.drawCircle(image.getWidth() / 2, image.getHeight() / 2,image.getWidth() / ((float) 2.4), paint);
			c.drawCircle(image.getWidth() / 2, image.getHeight() / 2,image.getWidth() / ((float) 2.4), strokePaint);
			return circle_bitmap;
		
	}
	
	public static Bitmap loadBitmap(String url) {
		Bitmap bm = null;
		try {
			  bm = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());
			} catch (MalformedURLException e) {
			  e.printStackTrace();
			} catch (IOException e) {
			  e.printStackTrace();
			}
		return bm;
	}
	
	public static boolean checkConn(Context context)
	{
        ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
       if(conMgr!=null)
       {
    	   NetworkInfo info = conMgr.getActiveNetworkInfo();
	        if(info==null)
	        	return false;
	        if(!info.isConnected())
	        	return false;
	        if(!info.isAvailable())
	        	return false;
			return true;
       }
       else
    	   return false;
        
	}
	
	public static String cropStringURL(String url)
	{
		String croppedString = null;
		int index = 0;
		int count = 0;
		
		for (int i = 4; i < url.length(); i++) {
			
			char c = url.charAt(i);
			if(c=='/')
			{
				count++;
				if(count==3)
					index = i;
			}
		}
		croppedString = url.substring(0, index+1);
		return croppedString;
	}
	
	public static double getDeviceInches(Activity activity)
	{
		DisplayMetrics dm = new DisplayMetrics();
	    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
	    double x = Math.pow(dm.widthPixels/dm.xdpi,2);
	    double y = Math.pow(dm.heightPixels/dm.ydpi,2);
	    double screenInches = Math.sqrt(x+y);
	    return screenInches;
	}

	public static String getDate() {

		Date now = new Date();
		Date alsoNow = Calendar.getInstance().getTime();
		String nowAsString = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
				.format(now);

		return nowAsString;
	}

}
