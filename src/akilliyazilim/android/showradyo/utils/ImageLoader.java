package akilliyazilim.android.showradyo.utils;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import akilliyazilim.android.showradyo.databases.MySQLiteOpenHelper;
import akilliyazilim.android.showradyo.model.DailyStreamListItem;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.analytics.tracking.android.Log;

public class ImageLoader {

	MemoryCache memoryCache = new MemoryCache();
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ExecutorService executorService;
	Context context;
	int status;
	int height, width;
	Activity activity;
	DailyStreamListItem item;
	String avatar_name;
	String programID;
	SQLiteDatabase db;
	MySQLiteOpenHelper helper;
	Cursor db_cursor;

	public ImageLoader(final Context context, Activity activity) {
		this.context = context;
		this.activity = activity;
		executorService = Executors.newFixedThreadPool(5);
	}

	public void DisplayImage(String avatar_name, ImageView imageView,
			int height, int width, DailyStreamListItem dailyStreamListItem,
			String programID) {
		this.width = width;
		this.height = height;
		this.item = dailyStreamListItem;
		this.avatar_name = avatar_name;
		this.programID = programID;

		imageViews.put(imageView, avatar_name);
		Bitmap bitmap = memoryCache.get(avatar_name);

		helper = new MySQLiteOpenHelper(context);
		db = helper.getWritableDatabase();
		db_cursor = db.query(MySQLiteOpenHelper.TABLE_NAME, null,
				MySQLiteOpenHelper.ID + " = ?",
				new String[] { this.programID }, null, null, null);
		if (db_cursor.moveToFirst()
				&& db_cursor.getCount() > 0
				&& !db_cursor
						.getString(
								db_cursor
										.getColumnIndex(MySQLiteOpenHelper.DjMobilImages))
						.equals("-")) {
			
			Bitmap bm = Utils.getCroppedImage(context, Utils.getResizedImage(
					context, ImageUtils.convertBase64ToImage(db_cursor.getString(db_cursor
							.getColumnIndex(MySQLiteOpenHelper.DjMobilImages))), height, width));
			imageView
			.setImageBitmap(bm);
			db.close();
		} else {
			if (bitmap != null) {
				imageView.setImageBitmap(bitmap);
			} else {
				queuePhoto(avatar_name, imageView, programID);
			}
		}
		db.close();
	}

	public void DisplayImage(String avatar_name, ImageView imageView,
			int height, int width, String programID) {
		this.width = width;
		this.height = height;
		this.programID = programID;

		imageViews.put(imageView, avatar_name);
		Bitmap bitmap = memoryCache.get(avatar_name);
		helper = new MySQLiteOpenHelper(context);
		db = helper.getWritableDatabase();
		db_cursor = db.query(MySQLiteOpenHelper.TABLE_NAME, null,
				MySQLiteOpenHelper.ID + " = ?",
				new String[] { this.programID }, null, null, null);
		if (db_cursor.moveToFirst()
				&& db_cursor.getCount() > 0
				&& !db_cursor
						.getString(
								db_cursor
										.getColumnIndex(MySQLiteOpenHelper.DjMobilImages))
						.equals("-")) {
			imageView
					.setImageBitmap(ImageUtils.convertBase64ToImage(db_cursor.getString(db_cursor
							.getColumnIndex(MySQLiteOpenHelper.DjMobilImages))));
			db.close();
		} else {
			if (bitmap != null)
				imageView.setImageBitmap(bitmap);
			else {
				queuePhoto(avatar_name, imageView, programID);
			}
		}
	}

	private void queuePhoto(String avatar_name, ImageView imageView,
			String programID) {
		PhotoToLoad p = new PhotoToLoad(avatar_name, imageView, programID);
		executorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String avatar_name) {
		// Bitmap bm =Utils.getCroppedImage(context,
		// Utils.getResizedImage(context, Integer.parseInt(avatar_name), 128,
		// 128));
		Bitmap bm = Utils.getCroppedImage(context, Utils.getResizedImage(
				context, Utils.loadBitmap(avatar_name), height, width));
		return bm;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String avatar_name;
		public ImageView imageView;
		public String daily_id;

		public PhotoToLoad(String u, ImageView i, String daily_id) {
			avatar_name = u;
			imageView = i;
			this.daily_id = daily_id;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				Bitmap bmp = getBitmap(photoToLoad.avatar_name);
				memoryCache.put(photoToLoad.avatar_name, bmp);
				if (imageViewReused(photoToLoad)) {
					return;
				}
				BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
				bd.update();
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.avatar_name))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void update() {
			activity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (imageViewReused(photoToLoad))
						return;
					if (bitmap != null) {
						photoToLoad.imageView.setImageBitmap(bitmap);

						helper = new MySQLiteOpenHelper(context);
						db = helper.getWritableDatabase();
						helper.update(ImageUtils.convertImageToBase64(bitmap),
								db, photoToLoad.daily_id);
						db.close();
					}
				}
			});
		}
	}

	public void clearCache() {
		memoryCache.clear();
	}

}
