package akilliyazilim.android.showradyo.playerservice;


import java.io.IOException;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.ShowRadyoActivity;
import akilliyazilim.android.showradyo.ShowRadyoTabletActivity;
import akilliyazilim.android.showradyo.utils.Utils;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioTrack;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

public class PlayerService extends Service implements PlayerCallback {

	MultiPlayer mediaPlayer;
	public static final int HELLO_ID = 1321;
	public static NotificationManager mNotificationManager;
	public static double screen_size;
	private static final String LOG = "SHOWRADYOSERVICE";

	private final IBinder mBinder = new LocalBinder();

	public class LocalBinder extends Binder {
		public PlayerService getService() {
			return PlayerService.this;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.v("onBind CALLED", "TRUE");
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		
		mediaPlayer.stop();
		mediaPlayer=null;
		mNotificationManager.cancel(HELLO_ID);
		return true;
	}

	public void getStartMediaPlayer(String url,double screen_size) throws IOException {
		
		try {
            java.net.URL.setURLStreamHandlerFactory( new java.net.URLStreamHandlerFactory(){
                public java.net.URLStreamHandler createURLStreamHandler( String protocol ) {
                    Log.d( LOG, "Asking for stream handler for protocol: '" + protocol + "'" );
                    if ("icy".equals( protocol )) return new com.spoledge.aacdecoder.IcyURLStreamHandler();
                    return null;
                }
            });
        }
        catch (Throwable t) {
            Log.w( LOG, "Cannot set the ICY URLStreamHandler - maybe already set ? - " + t );
        }
		
		this.screen_size=screen_size;
		String url_ =Utils.cropStringURL(url);
		mediaPlayer = new MultiPlayer(this, 500, 250);
		mediaPlayer.playAsync(url_);
		
	}

	public void getStopMediaPlayer() {
		if(mediaPlayer!=null)
		{
				stopSelf();
				mediaPlayer.stop();
				mediaPlayer=null;
				killNotification();
		}
	}

	public void initNotification() {
		Intent notificationIntent;
		String ns = Context.NOTIFICATION_SERVICE;
		mNotificationManager = (NotificationManager) getSystemService(ns);
		int icon = R.drawable.notification_icon;
		CharSequence tickerText = "Show Radyo";
		long when = System.currentTimeMillis();
		Context context = getApplicationContext();
		CharSequence contentTitle = "Show Radyo oynat�l�yor...";
		CharSequence contentText = "Bilgiler i�in t�klay�n�z.";

		if(screen_size<(5.65))
			notificationIntent = new Intent(this, ShowRadyoActivity.class);
		else
			notificationIntent = new Intent(this, ShowRadyoTabletActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		@SuppressWarnings("deprecation")
		Notification notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_NO_CLEAR;
		notification.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		mNotificationManager.notify(HELLO_ID, notification);
	}

	public void killNotification() {
		mNotificationManager.cancel(HELLO_ID);
	}

	@Override
	public void onDestroy() {
		mediaPlayer.stop();
		mediaPlayer=null;
		killNotification();
		stopSelf();
		super.onDestroy();
	}

	@Override
	public void playerStarted() {
		initNotification();
		
	}

	@Override
	public void playerPCMFeedBuffer(boolean isPlaying, int audioBufferSizeMs,
			int audioBufferCapacityMs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerStopped(int perf) {
		killNotification();
		
	}

	@Override
	public void playerException(Throwable t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMetadata(String key, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerAudioTrackCreated(AudioTrack audioTrack) {
		// TODO Auto-generated method stub
		
	}
	
}