package akilliyazilim.android.showradyo.fragments;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.model.CurrentDj;
import akilliyazilim.android.showradyo.model.DailyStreamListItem;
import akilliyazilim.android.showradyo.utils.ConstantsTwitter;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class CustomDialogTweet extends Dialog implements
android.view.View.OnClickListener {
	
	public Activity c;
	Dialog d;
	Button send,exit;
	EditText editText;
	String token, token_secret;
	CurrentDj dj;
	
	public CustomDialogTweet(Activity a,String token,String token_secret,CurrentDj dj) {
		super(a);
		this.token=token;
		this.token_secret=token_secret;
		this.c = a;
		this.dj = dj;
	}
	
	public CustomDialogTweet(Activity a,String token,String token_secret) {
		super(a);
		this.token=token;
		this.token_secret=token_secret;
		this.c = a;
		this.dj=null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.twitter_dialog_box);
		editText = (EditText) findViewById(R.id.twit_message);
		if(dj!=null)
			editText.setText(dj.getDjTwitterLink() +" "+dj.getHashTag() + " ");
		else
			editText.setText("@Show_Radyo ");
		send = (Button) findViewById(R.id.twit_send);
		exit = (Button) findViewById(R.id.twit_vazgec);
		send.setOnClickListener(this);
		exit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.twit_send:
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(ConstantsTwitter.CONSUMER_KEY);
			builder.setOAuthConsumerSecret(ConstantsTwitter.CONSUMER_SECRET);
			builder.setOAuthAccessToken(token);
			builder.setOAuthAccessTokenSecret(token_secret);

			AccessToken accessToken = new AccessToken(token, token_secret);
			Configuration configuration = builder.build();
			Twitter twitter = new TwitterFactory(configuration).getInstance(accessToken);
			try {
				twitter.updateStatus(editText.getText().toString());
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
			dismiss();
		case R.id.twit_vazgec:
			dismiss();
		}
		
	}
}
