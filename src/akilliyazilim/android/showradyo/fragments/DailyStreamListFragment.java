package akilliyazilim.android.showradyo.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.adapters.DailyStreamListAdapter;
import akilliyazilim.android.showradyo.databases.ApplicationPreferences;
import akilliyazilim.android.showradyo.databases.MySQLiteOpenHelper;
import akilliyazilim.android.showradyo.model.DailyStreamListItem;
import akilliyazilim.android.showradyo.utils.Utils;
import akilliyazilim.android.showradyo.webservicedata.DataService;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class DailyStreamListFragment extends Fragment implements
		OnClickListener {

	private static String chosen_day_pzt = "2013-12-16";
	private static String chosen_day_sal = "2013-12-17";
	private static String chosen_day_car = "2013-12-18";
	private static String chosen_day_prs = "2013-12-19";
	private static String chosen_day_cum = "2013-12-20";
	private static String chosen_day_cmt = "2013-12-21";
	private static String chosen_day_pzr = "2013-12-22";
	private static String chosen_day;

	Button button_pzt, button_sal, button_crs, button_prs, button_cum,
			button_cmt, button_pzr;
	ListView listview_daily_stream;
	DailyStreamListAdapter adapter_daily_stream;
	ArrayList<DailyStreamListItem> dailyStreamArrayList;
	String string_daily_stream;
	DailyStreamAsyncTask dailyStreamAsyncTask;

	public static final DailyStreamListFragment newInstance(String sampleText,
			Context c) {

		DailyStreamListFragment f = new DailyStreamListFragment();
		Bundle b = new Bundle();
		b.putString("bString", sampleText);
		f.setArguments(b);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_daily_stream,
				container, false);

		dailyStreamArrayList = new ArrayList<DailyStreamListItem>();

		button_pzt = (Button) view.findViewById(R.id.daily_pzt);
		button_sal = (Button) view.findViewById(R.id.daily_sal);
		button_crs = (Button) view.findViewById(R.id.daily_car);
		button_prs = (Button) view.findViewById(R.id.daily_prs);
		button_cum = (Button) view.findViewById(R.id.daily_cum);
		button_cmt = (Button) view.findViewById(R.id.daily_cmt);
		button_pzr = (Button) view.findViewById(R.id.daily_pzr);

		button_pzt.setOnClickListener(this);
		button_sal.setOnClickListener(this);
		button_crs.setOnClickListener(this);
		button_prs.setOnClickListener(this);
		button_cum.setOnClickListener(this);
		button_cmt.setOnClickListener(this);
		button_pzr.setOnClickListener(this);

		listview_daily_stream = (ListView) view
				.findViewById(R.id.listview_daily_stream);

		return view;
	}

	@Override
	public void onClick(View v) {
		int button_id = v.getId();
		if (button_id == R.id.daily_pzt) {
			if (chosen_day != chosen_day_pzt) {
				createDailyStreamAsyncTask(chosen_day_pzt);
				chosen_day = chosen_day_pzt;
			}
		} else if (button_id == R.id.daily_sal) {
			if (chosen_day != chosen_day_sal) {
				createDailyStreamAsyncTask(chosen_day_sal);
				chosen_day = chosen_day_sal;
			}

		} else if (button_id == R.id.daily_car) {
			if (chosen_day != chosen_day_car) {
				createDailyStreamAsyncTask(chosen_day_car);
				chosen_day = chosen_day_car;
			}

		} else if (button_id == R.id.daily_prs) {
			if (chosen_day != chosen_day_prs) {
				createDailyStreamAsyncTask(chosen_day_prs);
				chosen_day = chosen_day_prs;
			}

		} else if (button_id == R.id.daily_cum) {
			if (chosen_day != chosen_day_cum) {
				createDailyStreamAsyncTask(chosen_day_cum);
				chosen_day = chosen_day_cum;
			}

		} else if (button_id == R.id.daily_cmt) {
			if (chosen_day != chosen_day_cmt) {
				createDailyStreamAsyncTask(chosen_day_cmt);
				chosen_day = chosen_day_cmt;
			}
		} else {
			if (chosen_day != chosen_day_pzr) {

				createDailyStreamAsyncTask(chosen_day_pzr);
				chosen_day = chosen_day_pzr;
			}
		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO: handle exception
		if (Utils.checkConn(getActivity())) {
			// dailyStreamAsyncTask = new DailyStreamAsyncTask(getActivity()
			// .getApplicationContext(), chosen_day_pzt, "1");

			createDailyStreamAsyncTask(chosen_day_pzt);
			// createDailyStreamAsyncTask(chosen_day_sal);
			// createDailyStreamAsyncTask(chosen_day_car);
			// createDailyStreamAsyncTask(chosen_day_prs);
			// createDailyStreamAsyncTask(chosen_day_cum);
			// createDailyStreamAsyncTask(chosen_day_cmt);
			// createDailyStreamAsyncTask(chosen_day_pzr);
			chosen_day = chosen_day_pzt;
		} else {
			Toast.makeText(getActivity().getApplicationContext(),
					"Internet ba�lant�n�z� kontrol ediniz.", Toast.LENGTH_SHORT)
					.show();
		}
		chosen_day = chosen_day_pzt;

		Log.v("oncactivityCreted", "onActivityCreated");

		super.onActivityCreated(savedInstanceState);
	}

	public class DailyStreamAsyncTask extends
			AsyncTask<Void, Void, ArrayList<DailyStreamListItem>> {

		Context context;
		ArrayList<DailyStreamListItem> async_arraylist_dailystream;
		String today, CompanyID;

		// ProgressDialog progress_dialog;

		public DailyStreamAsyncTask(Context context, String today,
				String CompanyID) {
			this.context = context;
			this.today = today;
			this.CompanyID = CompanyID;
		}

		@Override
		protected void onPreExecute() {
			// progress_dialog = new ProgressDialog(getActivity());
			// progress_dialog.setIndeterminate(true);
			// progress_dialog.setIndeterminateDrawable(getResources()
			// .getDrawable(R.anim.progress_dialog_animation));
			// progress_dialog.setCancelable(false);
			// progress_dialog.setMessage("L�tfen bekleyiniz...");
			// progress_dialog.show();
			super.onPreExecute();
		}

		@Override
		protected ArrayList<DailyStreamListItem> doInBackground(Void... params) {
			Log.v("TODAY_DAY", today);
			async_arraylist_dailystream = DataService.getInstance()
					.getDailyStreamList(today, CompanyID);
			return async_arraylist_dailystream;
		}

		@Override
		protected void onPostExecute(ArrayList<DailyStreamListItem> result) {
			// progress_dialog.dismiss();
			dailyStreamArrayList = result;
			listview_daily_stream.setAdapter(new DailyStreamListAdapter(
					context, result, getActivity(), 0));
		}
	}

	public void createDailyStreamAsyncTask(String day) {

		MySQLiteOpenHelper helper = new MySQLiteOpenHelper(getActivity()
				.getApplicationContext());
		SQLiteDatabase db = helper.getWritableDatabase();
		if (calculateDateDifferences(
				ApplicationPreferences.getInstance(
						getActivity().getApplicationContext()).getDate(),
				Utils.getDate())) {
			helper.deleteAllRecords(db);
			ApplicationPreferences.getInstance(getActivity().getApplicationContext()).saveDate(Utils.getDate());
			Log.i("ShowRadyoActivity", ApplicationPreferences.getInstance(getActivity().getApplicationContext()).getDate() +"createDaily");
		}
		
		Cursor c = null;
		try {
			c = db.query(MySQLiteOpenHelper.TABLE_NAME, null, "day = ? ",
					new String[] { day }, null, null, null);
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (c != null && c.moveToFirst()) {
			ArrayList<DailyStreamListItem> items = helper.getDJ(db, day);
			listview_daily_stream.setAdapter(new DailyStreamListAdapter(
					getActivity().getApplicationContext(), items,
					getActivity(), 0));
		} else {
			if (Utils.checkConn(getActivity())) {
				dailyStreamAsyncTask = new DailyStreamAsyncTask(getActivity()
						.getApplicationContext(), day, "1");
				dailyStreamAsyncTask.execute();
			} else
				Toast.makeText(getActivity().getApplicationContext(),
						"Internet ba�lant�n�z� kontrol ediniz.",
						Toast.LENGTH_SHORT).show();
		}
		helper.close();

	}

	// get the current date

	/* get the date difference */
	private boolean calculateDateDifferences(String start, String end) {
		// TODO Auto-generated method stub
		String dateStart = start;
		String dateStop = end;
		long diffDays = 0;
		// HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);

			// in milliseconds
			long diff = d2.getTime() - d1.getTime();

			// long diffSeconds = diff / 1000 % 60;
			// long diffMinutes = diff / (60 * 1000) % 60;
			// long diffHours = diff / (60 * 60 * 1000) % 24;
			diffDays = diff / (24 * 60 * 60 * 1000);

			// System.out.print(diffDays + " days, ");
			// System.out.print(diffHours + " hours, ");
			// System.out.print(diffMinutes + " minutes, ");
			// System.out.print(diffSeconds + " seconds.");

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (diffDays < 7)
			return false;
		else
			return true;

	}
}
