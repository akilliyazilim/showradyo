package akilliyazilim.android.showradyo.fragments;

import java.util.ArrayList;
import java.util.Calendar;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.TwitterActivity;
import akilliyazilim.android.showradyo.adapters.TwitterListAdapter;
import akilliyazilim.android.showradyo.model.Tweet;
import akilliyazilim.android.showradyo.utils.ConstantsTwitter;
import akilliyazilim.android.showradyo.utils.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.costum.android.widget.PullAndLoadListView;
import com.costum.android.widget.PullAndLoadListView.OnLoadMoreListener;
import com.costum.android.widget.PullToRefreshListView.OnRefreshListener;

public class TweetListFragment extends Fragment {

	ArrayList<Tweet> tweet;
	ArrayList<Tweet> temp;
//	ProgressDialog pd;
	ListView lv;
	TwitterListAdapter adapter;
	int morePage = 1;
	Twitter mTwitter;
	RequestToken mRequestToken;
	SharedPreferences pref;
	TextView twit_message_send;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_tweets,container, false);
		twit_message_send = (TextView) view.findViewById(R.id.twit_list_message_send);
		pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,getActivity().MODE_PRIVATE);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		new AsyncTaskClass().execute();
		
		twit_message_send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utils.checkConn(getActivity()))
				{
						if (IsLogin()) {
							pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,getActivity().MODE_PRIVATE);
							String access_token = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN, "");
							String access_token_secret = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET, "");
							CustomDialogTweet cdd = new CustomDialogTweet(getActivity(), access_token, access_token_secret);
							cdd.show();
						}
						else 
						{
							Login();
						}
				}
				
			}
		});
		super.onActivityCreated(savedInstanceState);
	}

	class AsyncTaskClass extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
//			pd = new ProgressDialog(getActivity());
//			pd.setTitle("Processing...");
//			pd.setMessage("Please wait.");
//			pd.setCancelable(false);
//			pd.setIndeterminate(true);
//			pd.show();

		}

		@Override
		protected String doInBackground(String... strings) {

			// uzun islem sirasinda yapilacaklar
			tweet = getTweets(1, 30);
			return null;
		}

		@Override
		protected void onPostExecute(String s) {
			// uzun islem bitince yapilacaklar

			setTweet(tweet);
//			pd.dismiss();

			super.onPostExecute(s);
		}
	}

	public void setTweet(ArrayList<Tweet> arrayListTweet) {

		lv = (PullAndLoadListView) getView().findViewById(R.id.pulltorefresh);
		adapter = new TwitterListAdapter(getActivity(), arrayListTweet);
		lv.setAdapter(adapter);

		((PullAndLoadListView) lv)
				.setOnLoadMoreListener(new OnLoadMoreListener() {
					public void onLoadMore() {
						// Do the work to load more items at the end of list
						// here
						morePage++;
						new LoadMoreDataTask().execute();
					}
				});

		((PullAndLoadListView) lv)
				.setOnRefreshListener(new OnRefreshListener() {
					public void onRefresh() {
						// Do work to refresh the list here.
						new PullToRefreshDataTask().execute();
					}
				});
	}

	private class LoadMoreDataTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {

			tweet.addAll(getTweets(morePage, 30));
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// We need notify the adapter that the data have been changed
			adapter.notifyDataSetChanged();
			// Call onLoadMoreComplete when the LoadMore task, has finished
			((PullAndLoadListView) lv).onLoadMoreComplete();
			super.onPostExecute(result);
		}
	}

	private class PullToRefreshDataTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			tweet = getTweets(1, 30);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			setTweet(tweet);
			// We need notify the adapter that the data have been changed
			adapter.notifyDataSetChanged();
			// Call onLoadMoreComplete when the LoadMore task, has finished
			((PullAndLoadListView) lv).onRefreshComplete();

			super.onPostExecute(result);
		}
	}

	public ArrayList<Tweet> getTweets(int tweet_page, int tweet_size) {

		String token = "550325929-BVxxlhDq7IV3c5HGjLki8ZjEfGlTOB8jfiHbyzpF";
		String secret = "EX9oqzCn94nkPOKkOFmIcoCCvVuJDmpBrHZWurSMqWQEZ";
		AccessToken a = new AccessToken(token, secret);
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer("cpIsLwzkrZbMQeIZBzS3zA",
				"53xrDs1reysrOrQ2GB2I4VftygED7eH7bHtzonKxjuM");
		twitter.setOAuthAccessToken(a);

		Paging p = new Paging(tweet_page, tweet_size);

		try {
			ResponseList<Status> sta = twitter.getUserTimeline("Show_Radyo", p);

			Tweet t = null;
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH) + 1;
			int day = c.get(Calendar.DAY_OF_MONTH);
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minutes = c.get(Calendar.MINUTE);
			int second = c.get(Calendar.SECOND);

			ArrayList<Tweet> statusTexts = new ArrayList<Tweet>();
			for (Status s : sta) {
				t = new Tweet();
				t.setTweet(s.getText());
				if ((s.getCreatedAt().getYear() + 1900) == year
						&& (s.getCreatedAt().getMonth() + 1) == month
						&& (s.getCreatedAt().getDate()) == day) {

					if (s.getCreatedAt().getHours() == hour
							&& s.getCreatedAt().getMinutes() == minutes) {

						t.setTweet_date((second - s.getCreatedAt().getSeconds())
								+ " saniye �nce");
					} else if (s.getCreatedAt().getHours() == hour) {

						t.setTweet_date((minutes - s.getCreatedAt()
								.getMinutes()) + " dakika �nce");
					} else {

						t.setTweet_date(hour - s.getCreatedAt().getHours()
								+ " saat �nce");
					}
				} else if ((s.getCreatedAt().getYear() + 1900) != year) {

					t.setTweet_date(s.getCreatedAt().getDate() + " "
							+ getMonth1(s.getCreatedAt().getMonth() + 1) + " "
							+ (s.getCreatedAt().getYear() + 1900));
				} else {

					t.setTweet_date(s.getCreatedAt().getDate() + " "
							+ getMonth1(s.getCreatedAt().getMonth() + 1));
				}

				statusTexts.add(t);
			}
			return statusTexts;
		} catch (Exception e1) {
			return null;
		}
	}

	public String getMonth1(int month) {

		switch (month) {
		case 1:
			return "Ocak";

		case 2:
			return "�ubat";

		case 3:
			return "Mart";

		case 4:
			return "Nisan";

		case 5:
			return "May�s";

		case 6:
			return "Haziran";

		case 7:
			return "Temmuz";

		case 8:
			return "Agustos";

		case 9:
			return "Eyl�l";

		case 10:
			return "Ekim";

		case 11:
			return "Kas�m";

		default:
			return "Aral�k";

		}

	}

	public static final TweetListFragment newInstance(String sampleText) {

		TweetListFragment f = new TweetListFragment();
		Bundle b = new Bundle();
		b.putString("bString", sampleText);
		f.setArguments(b);

		return f;
	}
	
	public void Login() {

			ConfigurationBuilder confbuilder = new ConfigurationBuilder();
			Configuration conf = confbuilder
					.setOAuthConsumerKey(ConstantsTwitter.CONSUMER_KEY)
					.setOAuthConsumerSecret(ConstantsTwitter.CONSUMER_SECRET).build();
			mTwitter = new TwitterFactory(conf).getInstance();
			mTwitter.setOAuthAccessToken(null);
			try {
				mRequestToken = mTwitter.getOAuthRequestToken(ConstantsTwitter.CALLBACK_URL);
				Intent intent = new Intent(getActivity(), TwitterActivity.class);
				intent.putExtra(ConstantsTwitter.IEXTRA_AUTH_URL,
						mRequestToken.getAuthorizationURL());
				startActivityForResult(intent, 0);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

		}
	    
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
			super.onActivityResult(requestCode, resultCode, intent);
			if (requestCode == 0) {
				if (resultCode == Activity.RESULT_OK) {
					AccessToken accessToken = null;
					try {
						String oauthVerifier = intent.getExtras().getString(
								ConstantsTwitter.IEXTRA_OAUTH_VERIFIER);
						accessToken = mTwitter.getOAuthAccessToken(mRequestToken,
								oauthVerifier);
						SharedPreferences pref = getActivity()
								.getSharedPreferences(ConstantsTwitter.PREF_NAME,
										getActivity().MODE_PRIVATE);
						SharedPreferences.Editor editor = pref.edit();
						editor.putString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN,
								accessToken.getToken());
						editor.putString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET,
								accessToken.getTokenSecret());
						editor.commit();

					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (resultCode == getActivity().RESULT_CANCELED) {
					Log.w("tag", "��lem yar�m kald�!");
				}
			}
		}
	    
	public boolean IsLogin() {
			if (pref.contains(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN)) {
				pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,
						getActivity().MODE_PRIVATE);
				// Access Token
				String access_token = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN,
						"");
				// Access Token Secret
				String access_token_secret = pref.getString(
						ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET, "");

				if (access_token.equals("") && access_token_secret.equals("")) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

}
