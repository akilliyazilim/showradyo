package akilliyazilim.android.showradyo.fragments;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.utils.Utils;
import akilliyazilim.android.showradyo.webservicedata.DataService;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MessageFragment extends Fragment{
	
	EditText edittext_ad_soyad,edittext_mail,edittext_message_content;
	TextView textview_send;

	public static final MessageFragment newInstance(String sampleText) {
		
		MessageFragment f = new MessageFragment();
        Bundle b = new Bundle();
        b.putString("bString", sampleText);
        f.setArguments(b);
        return f;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view =inflater.inflate(R.layout.layout_fragment_message_form, container,false);
		edittext_ad_soyad = (EditText) view.findViewById(R.id.edittext_ad_soyad);
		edittext_mail = (EditText) view.findViewById(R.id.edittext_mail);
		edittext_message_content = (EditText) view.findViewById(R.id.message_content);
		textview_send = (TextView) view.findViewById(R.id.text_message_send);
		
		textview_send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utils.checkConn(getActivity()))
				{
					WifiManager manager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
					WifiInfo info = manager.getConnectionInfo();
					String address = info.getMacAddress();
					if(edittext_ad_soyad.getText().length()>4 && edittext_mail.getText().length()>8 && edittext_message_content.getText().length()>15)
					{
						MessageSendAsyncTask messageSendAsync = new MessageSendAsyncTask(getActivity().getApplicationContext(), edittext_ad_soyad.getText().toString(), edittext_message_content.getText().toString(), edittext_mail.getText().toString(),address);
						messageSendAsync.execute();
					}
					else
						Toast.makeText(getActivity().getApplicationContext(), "Bo�luklar� do�ru bir �ekilde doldurunuz.", Toast.LENGTH_SHORT).show();
						
				}
				else
					Toast.makeText(getActivity().getApplicationContext(), "Internet ba�lant�n�z� kontrol ediniz.", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	public class MessageSendAsyncTask extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog progress_dialog;
		String title,messageContent,mailAdress,deviceID;
		Context context;
		boolean message_return_value;

		public MessageSendAsyncTask(Context context,String title,String messageContent,String mailAdress,String deviceID) {
			this.context=context;
			this.title=title;
			this.messageContent=messageContent;
			this.mailAdress=mailAdress;
			this.deviceID=deviceID;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {

			message_return_value = DataService.getInstance().sendMessageForm("1", "Show Radyo", title, messageContent, mailAdress, deviceID);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(message_return_value)
				Toast.makeText(context, "Mesaj�n�z g�nderildi.", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(context, "Mesaj�n�z g�nderilemedi.", Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
		
	}

}
