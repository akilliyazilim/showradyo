package akilliyazilim.android.showradyo.fragments;

import java.util.ArrayList;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.json.JSONException;

import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.adapters.FrequencyCityWheelAdapter;
import akilliyazilim.android.showradyo.model.FrequencyListItem;
import akilliyazilim.android.showradyo.utils.Utils;
import akilliyazilim.android.showradyo.webservicedata.DataService;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class FrequencyListFragment extends Fragment {
	
	boolean scrolling = false;
	WheelView city_wheel;
	TextView textview_frequency,textview_city;
	ArrayList<FrequencyListItem> frequencyItemsList;
	FrequencyCityWheelAdapter frequencyCityListAdapter;

	public static final FrequencyListFragment newInstance(String sampleText) {
		
		FrequencyListFragment f = new FrequencyListFragment();
        Bundle b = new Bundle();
        b.putString("bString", sampleText);
        f.setArguments(b);
        return f;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		if(Utils.checkConn(getActivity()))
		{
			FrequencyListAsyncTask freqAsyncTask = new FrequencyListAsyncTask(getActivity().getApplicationContext(), "1");
			freqAsyncTask.execute();
		}
		else
			Toast.makeText(getActivity().getApplicationContext(), "Internet ba�lant�n�z� kontrol ediniz.", Toast.LENGTH_SHORT).show();
		
		
		
		city_wheel.addChangingListener(new OnWheelChangedListener() {
			
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if(!scrolling)
				{
					Log.v("ONCHANGED", String.valueOf(newValue));
					textview_city.setText(frequencyItemsList.get(newValue).getCityName());
					textview_frequency.setText(frequencyItemsList.get(newValue).getFrequencyNumber());
				}
			}
		});
		
		city_wheel.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				scrolling=true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				scrolling=false;
				textview_city.setText(frequencyItemsList.get(city_wheel.getCurrentItem()).getCityName());
				textview_frequency.setText(frequencyItemsList.get(city_wheel.getCurrentItem()).getFrequencyNumber());
			}
		});
		
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_fragment_frequency, container,false);
		city_wheel = (WheelView) view.findViewById(R.id.wheel_frequency);
		textview_city = (TextView) view.findViewById(R.id.textview_frequency_city);
		textview_frequency = (TextView) view.findViewById(R.id.textview_frequency_number);
		
		
		return view;
	}
	
	public class FrequencyListAsyncTask extends AsyncTask<Void, Void, ArrayList<FrequencyListItem>>
	{

		Context context;
		String companyID;
		ArrayList<FrequencyListItem> freqArrayList;
//		ProgressDialog progress_dialog;
		
		public FrequencyListAsyncTask(Context context,String companyID) {
			this.context=context;
			this.companyID=companyID;
		}
		
		@Override
		protected void onPreExecute() {
//			progress_dialog= new ProgressDialog(getActivity());
//			progress_dialog.setIndeterminate(true);
//			progress_dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_animation));
//			progress_dialog.setCancelable(false);
//			progress_dialog.setMessage("L�tfen bekleyiniz...");
//			progress_dialog.show();
			super.onPreExecute();
		}
		
		@Override
		protected ArrayList<FrequencyListItem> doInBackground(Void... params) {
			try {
				freqArrayList = DataService.getInstance().getFrequencyList(companyID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return freqArrayList;
		}
		
		@Override
		protected void onPostExecute(ArrayList<FrequencyListItem> result) {
//			progress_dialog.dismiss();
			frequencyItemsList = result;
			city_wheel.setViewAdapter(new FrequencyCityWheelAdapter(context,result));
			city_wheel.setVisibleItems(5);
			city_wheel.setCurrentItem(5);
			super.onPostExecute(result);
		}
		
	}

}
