package akilliyazilim.android.showradyo.fragments;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import uk.co.senab.photoview.PhotoViewAttacher;
import akilliyazilim.android.showradyo.R;
import akilliyazilim.android.showradyo.SuggestionMail;
import akilliyazilim.android.showradyo.TwitterActivity;
import akilliyazilim.android.showradyo.libraries.Blur;
import akilliyazilim.android.showradyo.libraries.ImageUtils;
import akilliyazilim.android.showradyo.model.CurrentDj;
import akilliyazilim.android.showradyo.model.LiveSongNameAndSinger;
import akilliyazilim.android.showradyo.model.LiveStreamInfo;
import akilliyazilim.android.showradyo.playerservice.PlayerService;
import akilliyazilim.android.showradyo.playerservice.PlayerService.LocalBinder;
import akilliyazilim.android.showradyo.utils.ConstantsTwitter;
import akilliyazilim.android.showradyo.utils.ImageLoader;
import akilliyazilim.android.showradyo.utils.Utils;
import akilliyazilim.android.showradyo.webservicedata.DataService;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.Build.VERSION;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class LiveStreamFragment extends Fragment{
	
	private static final int TOP_HEIGHT = 700;
	TextView textview_song,textview_singer,textview_program_name;
	String string_dj_image_link,string_dj_twitter_link;
	ImageView imageview_control,imageview_dj_image,imageview_twitter,imageview_info_icon;
	SeekBar seekbar_volume_control;
	LiveStreamInfo live_stream_info;
	CurrentDj current_dj_info;
	LiveSongNameAndSinger current_song_singer;
	PlayerService mService;
	AudioManager audioManager;
	boolean mBound,isDataReady;
	LiveStreamAsyncTask liveStreamAsyncTask;
	Timer timer;
	TimerTask timer_task;
	LinearLayout linear_layout;
	ImageView mBlurredImage;
	PhotoViewAttacher mAttacher;
	View headerView;
	float alpha_float = 1;
	int alpha_int = 1;
	Bitmap bmp;
	int screenWidth,screenHeight; 
	Intent intent ;
	Twitter mTwitter;
	RequestToken mRequestToken;
	SharedPreferences pref;
	public static NotificationManager mNotificationManager;

	public static final LiveStreamFragment newInstance(String sampleText) {
		LiveStreamFragment f = new LiveStreamFragment();
        Bundle b = new Bundle();
        b.putString("bString", sampleText);
        f.setArguments(b);
        return f;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String ns = Context.NOTIFICATION_SERVICE;
		mNotificationManager = (NotificationManager) getActivity().getApplicationContext().getSystemService(ns);
		mNotificationManager.cancel(PlayerService.HELLO_ID);

		View view =inflater.inflate(R.layout.layout_fragment_live_stream, container,false);
		
		pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,getActivity().MODE_PRIVATE);
		
	    mBound = false;
	    audioManager = (AudioManager) getActivity().getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		textview_program_name = (TextView) view.findViewById(R.id.textview_live_program_name);
		textview_singer = (TextView) view.findViewById(R.id.textview_live_singer_name);
		textview_song = (TextView) view.findViewById(R.id.textview_live_song_name);
		imageview_control = (ImageView) view.findViewById(R.id.imageview_live_control);
		imageview_dj_image = (ImageView) view.findViewById(R.id.imageview_live_dj_image);
		imageview_twitter = (ImageView) view.findViewById(R.id.imageview_live_dj_twitter);
		seekbar_volume_control = (SeekBar) view.findViewById(R.id.volume_seekbar);
		imageview_info_icon = (ImageView) view.findViewById(R.id.imageview_info);
		
		seekbar_volume_control.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		seekbar_volume_control.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
		
		imageview_control.setBackgroundResource(R.drawable.button_play);
		

        mBlurredImage = (ImageView) view.findViewById(R.id.iv_photo);
        mAttacher = new PhotoViewAttacher(mBlurredImage);

       if(VERSION.SDK_INT>11)
     	  mBlurredImage.setAlpha(alpha_float);
        else
        	mBlurredImage.setAlpha(alpha_int);
		headerView = new View(getActivity().getApplicationContext());
		headerView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, TOP_HEIGHT));

		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		
		screenWidth = ImageUtils.getScreenWidth(getActivity());
		screenHeight = ImageUtils.getScreenHeight(getActivity());
		
		if(Utils.checkConn(getActivity()))
		{
			liveStreamAsyncTask = new LiveStreamAsyncTask(getActivity().getApplicationContext(), "1");
			liveStreamAsyncTask.execute();
		}
		else
			Toast.makeText(getActivity().getApplicationContext(), "Internet ba�lant�n�z� kontrol ediniz.", Toast.LENGTH_SHORT).show();
		
		
		timer = new Timer();
		timer_task = new TimerTask() {
			
			@Override
			public void run() {
				if(Utils.checkConn(getActivity()))
					new LiveStreamAsyncTask(getActivity().getApplicationContext(), "1").execute();
			}
		};
		timer.scheduleAtFixedRate(timer_task, 20*1000L, 20*1000L);
		
		seekbar_volume_control.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
				
			}
		});
		
		imageview_control.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(isDataReady)
				{
					if(mBound)
					{
						mBound=false;
						getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
						try {
							double screen_size = Utils.getDeviceInches(getActivity());
							mService.getStartMediaPlayer(live_stream_info.getLiveStreamLink(),screen_size);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						imageview_control.setBackgroundResource(R.drawable.button_pause);
						
					}
					else
					{
						mBound=true;
						mService.getStopMediaPlayer();
						imageview_control.setBackgroundResource(R.drawable.button_play);
					}
				}
			}
		});
		
		imageview_twitter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utils.checkConn(getActivity()))
				{
						if (IsLogin()) {
							pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,getActivity().MODE_PRIVATE);
							String access_token = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN, "");
							String access_token_secret = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET, "");
							CustomDialogTweet cdd = new CustomDialogTweet(getActivity(), access_token, access_token_secret,current_dj_info);
					
						cdd.show();
	
						} else {
							Login();
						}
				}
			}
		});
		
		imageview_info_icon.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(),SuggestionMail.class);
				startActivity(i);
			}
		});
		
		super.onActivityCreated(savedInstanceState);
	}
	
	public class LiveStreamAsyncTask extends AsyncTask<Void, Void, Void>
	{
		Context context;
		String CompanyID;
		ImageLoader image_loader;
		CurrentDj temp_current_dj_info;
		LiveSongNameAndSinger temp_current_song_singer;
		LiveStreamInfo temp_live_stream_info;
		
		
		
		public LiveStreamAsyncTask(Context context,String CompanyID) {
			this.context=context;
			this.CompanyID=CompanyID;
			image_loader = new ImageLoader(context,getActivity());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			temp_current_dj_info = DataService.getInstance().getCurrentDjInfo();
			temp_current_song_singer = DataService.getInstance().getLiveSongNameAndSinger();
			temp_live_stream_info = DataService.getInstance().getLiveStreamInfo(CompanyID);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			isDataReady=true;
			Log.v("GELIYO", "GELIYO1");
			
			if(current_dj_info==null || current_song_singer==null || temp_live_stream_info==null)
			{
				if(temp_current_dj_info!=null && temp_current_song_singer!=null && temp_live_stream_info!=null)
				{
					Log.v("GELIYO", "GELIYO2");
					current_dj_info=temp_current_dj_info;
					current_song_singer = temp_current_song_singer;
					live_stream_info = temp_live_stream_info;
					textview_program_name.setText(current_dj_info.getProgramName().trim());
					string_dj_twitter_link=current_dj_info.getDjTwitterLink();
					string_dj_image_link = current_dj_info.getDjMobilImages();
					textview_singer.setText(current_song_singer.getSingerName().trim());
					textview_song.setText(current_song_singer.getSongName().trim());
					
					image_loader.DisplayImage(string_dj_image_link, imageview_dj_image, 240, 240,"-1");
					
					UpdateBackgroundBlurAsyncTask update_async = new UpdateBackgroundBlurAsyncTask(context);
					update_async.execute();
				}
				
			}
			else
			{
				if(temp_current_dj_info!=null && temp_current_song_singer!=null && temp_live_stream_info!=null)
				{
					if(!ControlDjInfo(current_dj_info, temp_current_dj_info))
					{
						Log.v("GELIYO", "GELIYO3");
						Log.v("DJ CHANGED!!", "DJ CHANGED!!!");
						current_dj_info=temp_current_dj_info;
						textview_program_name.setText(current_dj_info.getProgramName().trim());
						string_dj_twitter_link=current_dj_info.getDjTwitterLink();
						string_dj_image_link = current_dj_info.getDjMobilImages();
						image_loader.DisplayImage(string_dj_image_link, imageview_dj_image, 240, 240,"-1");
						UpdateBackgroundBlurAsyncTask update_async = new UpdateBackgroundBlurAsyncTask(context);
						update_async.execute();
					}
					if(!ControlCurrentSongInfo(current_song_singer, temp_current_song_singer))
					{
						Log.v("GELIYO", "GELIYO4");
						Log.v("MUSIC CHANGED!!", "MUSIC CHANGED!!!");
						current_song_singer = temp_current_song_singer;
						textview_singer.setText(current_song_singer.getSingerName().trim());
						textview_song.setText(current_song_singer.getSongName().trim());
					}
					if(!ControlLiveStreamInfo(live_stream_info, temp_live_stream_info))
					{
						Log.v("GELIYO", "GELIYO5");
						Log.v("STERAM CHANGED!!", "STREAM CHANGED!!!");
						live_stream_info = temp_live_stream_info;
					}
				}
			}
			super.onPostExecute(result);
		}
	}
	
	public class UpdateBackgroundBlurAsyncTask extends AsyncTask<Void, Void, Void>
	{
		Context context;
		Bitmap image,newImg;
		BitmapFactory.Options options;
		
		public UpdateBackgroundBlurAsyncTask(Context context) {
			this.context=context;
		}
		@Override
		protected void onPreExecute() {
			options = new BitmapFactory.Options();
			options.inSampleSize = 2;
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			image = Utils.loadBitmap(current_dj_info.getDjImage());
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			newImg = Blur.fastblur(context, image, 12);
			updateView(screenWidth, newImg);
			super.onPostExecute(result);
		}
		
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			LocalBinder binder = (LocalBinder) service;
            mService = binder.getService();
            Log.v("onServiceConnected", "onServiceConnected");
            mBound = true;
			
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
		}
    };
    
    private void updateView(final int screenWidth,Bitmap bmpBlurred ) {
		bmpBlurred = Bitmap.createScaledBitmap(bmpBlurred, screenWidth,(int) (bmpBlurred.getHeight()
				* ((float) screenWidth) / (float) bmpBlurred.getWidth()), false);
		mBlurredImage.setImageBitmap(bmpBlurred);
		mBlurredImage.setScaleType(ScaleType.CENTER_CROP);
	}
    
    @Override
    public void onStart() {
        super.onStart();
        Log.v("onStart", "onStart");
        intent = new Intent(getActivity().getApplicationContext(), PlayerService.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    }
	
	@Override
	public void onDestroy() {
        if (mBound) {
    		mService.killNotification();
            getActivity().unbindService(mConnection);
            mBound = false;
        }
    	super.onDestroy();
    }
	
    @Override
    public void onSaveInstanceState(Bundle toSave) {
      super.onSaveInstanceState(toSave);
      toSave.putParcelable("bitmap", bmp);
    }

    public void Login() {

		ConfigurationBuilder confbuilder = new ConfigurationBuilder();
		Configuration conf = confbuilder
				.setOAuthConsumerKey(ConstantsTwitter.CONSUMER_KEY)
				.setOAuthConsumerSecret(ConstantsTwitter.CONSUMER_SECRET).build();
		mTwitter = new TwitterFactory(conf).getInstance();
		mTwitter.setOAuthAccessToken(null);
		try {
			mRequestToken = mTwitter.getOAuthRequestToken(ConstantsTwitter.CALLBACK_URL);
			Intent intent = new Intent(getActivity(), TwitterActivity.class);
			intent.putExtra(ConstantsTwitter.IEXTRA_AUTH_URL,
					mRequestToken.getAuthorizationURL());
			startActivityForResult(intent, 0);
		} catch (TwitterException e) {
			e.printStackTrace();
		}

	}
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == 0) {
			if (resultCode == Activity.RESULT_OK) {
				AccessToken accessToken = null;
				try {
					String oauthVerifier = intent.getExtras().getString(
							ConstantsTwitter.IEXTRA_OAUTH_VERIFIER);
					accessToken = mTwitter.getOAuthAccessToken(mRequestToken,
							oauthVerifier);
					SharedPreferences pref = getActivity()
							.getSharedPreferences(ConstantsTwitter.PREF_NAME,
									getActivity().MODE_PRIVATE);
					SharedPreferences.Editor editor = pref.edit();
					editor.putString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN,
							accessToken.getToken());
					editor.putString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET,
							accessToken.getTokenSecret());
					editor.commit();

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (resultCode == getActivity().RESULT_CANCELED) {
				Log.w("tag", "��lem yar�m kald�!");
			}
		}
	}
    
    public boolean IsLogin() {
		if (pref.contains(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN)) {
			pref = getActivity().getSharedPreferences(ConstantsTwitter.PREF_NAME,
					getActivity().MODE_PRIVATE);
			// Access Token
			String access_token = pref.getString(ConstantsTwitter.PREF_KEY_ACCESS_TOKEN,
					"");
			// Access Token Secret
			String access_token_secret = pref.getString(
					ConstantsTwitter.PREF_KEY_ACCESS_TOKEN_SECRET, "");

			if (access_token.equals("") && access_token_secret.equals("")) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

    public boolean ControlDjInfo(CurrentDj old_one,CurrentDj new_one)
    {
    	if(old_one.getCurrentDjID()!= new_one.getCurrentDjID())
    		return false;
    	else if(!old_one.getDjName().equals(new_one.getDjName()))
    		return false;
    	else
    		return true;
    }
    
    public boolean ControlCurrentSongInfo(LiveSongNameAndSinger old_one,LiveSongNameAndSinger new_one)
    {
    	if(!old_one.getSingerName().equals(new_one.getSingerName()))
    		return false;
    	else if(!old_one.getSongName().equals(new_one.getSongName()))
    		return false;
    	else
    		return true;
    }
    
    public boolean ControlLiveStreamInfo(LiveStreamInfo old_one,LiveStreamInfo new_one)
    {
    	if(old_one.getLiveID()!=new_one.getLiveID())
    		return false;
    	else if(!old_one.getLiveStreamLink().equals(new_one.getLiveStreamLink()))
    		return false;
    	else
    		return true;
    }
}
